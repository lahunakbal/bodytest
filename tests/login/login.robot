***Settings***
Documentation   Suite de testes de login do administrador

Resource            ${EXECDIR}/resources/base.robot

Suite Setup         Start Browser Session
Test Teardown       Take Screenshot
#Test Setup
#Suite Teardown

***Test Cases***
Cenario: Login do administrador
    [Tags]      login
    [Teardown]                      Clear LS
    Go To Login Page
    Login With                 admin@bodytest.com     pwd123
    User Should Be Logged In        Administrador
    

Cenario: Senha incorreta
    [Tags]      
    [Teardown]                  ThinkTime
    Go To Login Page
    Login With                  admin@bodytest.com     abc123
    Toaster Text Should Be      Usuário e/ou senha inválidos.
    

Cenario: Email não cadastrado
    [Tags]      
    [Teardown]                  ThinkTime
    Go To Login Page
    Login With                  leo.ferreira@bodytest.com     abc123
    Toaster Text Should Be      Usuário e/ou senha inválidos.
    

Cenario: mail incorreto
    [Tags]      
    Go To Login Page
    Login With                  admin&bodytest.com      pwd123
    Alert Text Should Be        Informe um e-mail válido  

Cenario: Senha não informada
    [Tags]      
    Go To Login Page
    Login With                  admin&bodytest.com      ${EMPTY}
    Alert Text Should Be        A senha é obrigatória  

Cenario: Email não informado
    [Tags]      
    Go To Login Page
    Login With                  ${EMPTY}      abc123
    Alert Text Should Be        O e-mail é obrigatório

Cenario: Email e senha não informados
    [Tags]      
    Go To Login Page
    Login With                  ${EMPTY}        ${EMPTY}
    Alert Text Should Be        O e-mail é obrigatório
    Alert Text Should Be        A senha é obrigatória   