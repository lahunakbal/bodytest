***Settings***
Documentation       Suíte de Cadastro de Alunos

Resource            ${EXECDIR}/resources/base.robot

Suite Setup         User Session
Test Teardown       Take Screenshot

***Test Cases***
Cenario: Novo Aluno
    [Tags]      new
    [Teardown]      ThinkTime
    &{student}      Create Dictionary   name=Leandro Ferreira      email=leo.ferreira@mail.io     age=34       weight=74      height=1.68 

    Remove Student             ${student.email}
    Go To Students
    Go To Student Form
    New Student                ${student}        

    #Toaster de Sucesso
    #<div role="alert" class="Toastify__toast-body">Aluno cadastrado com sucesso.</div>
    Toaster Text Should Be         Aluno cadastrado com sucesso.

Cenario: Não deve permitir email duplicado
    [Tags]      dup
    [Teardown]       ThinkTime
    &{student}      Create Dictionary   name=Leandro Ferreira      email=leo.ferreira@mail.io     age=34       weight=74      height=1.68 

    Insert Student            ${student}
    Go To Students
    Go To Student Form
    New Student               ${student}        

    Toaster Text Should Be    Email já existe no sistema.

Cenario: Todos os campos devem ser obrigatorios
    [Tags]      req
    @{expect_alerts}                Set Variable        Nome é obrigatório      O e-mail é obrigatório      idade é obrigatória     o peso é obrigatório    a Altura é obrigatória
    @{got_alerts}                   Create List         

    Go To Students
    Go To Student Form
    Submit Student Form

#    FOR         ${alert}    IN      @{expect_alerts}
#    Alert Text Should Be            ${alert}                     
#    END

    FOR     ${index}        IN RANGE    1   6
        ${span}             Get Required Alerts     ${index}
        Append To List      ${got_alerts}          ${span}
    END

    Log     ${expect_alerts}  
    Log     ${got_alerts}  

    Lists Should Be Equal   ${expect_alerts}        ${got_alerts} 

Cenario: Validacao dos campos numericos
    [Tags]      number
    [Template]      Check Field Type On Student Form
    ${AGE_FIELD}          number
    ${WEIGHT_FIELD}       number
    ${HEIGHT_FIELD}       number

Cenario: Validar campo do tipo email
    [Tags]      email
    [Template]      Check Field Type On Student Form
    ${EMAIL_FIELD}        email

Cenario: Validar idade mínima para cadastro
    [Tags]      age
    &{student}      Create Dictionary   name=Leandro Ferreira      email=leo.ferreira@mail.io     age=13       weight=56      height=1.59 

    Go To Students
    Go To Student Form
    New Student             ${student}        
    Alert Text Should Be    A idade deve ser maior ou igual 14 anos


***Keywords***
Check Field Type On Student Form
    [Arguments]             ${element}      ${type}

    Go To Students
    Go To Student Form
    Field Type Should Be     ${element}      ${type}
