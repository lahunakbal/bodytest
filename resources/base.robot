***Settings***
Documentation      Tudo comeca aqui, meu arquivo base do projeto de automação BodyTest

Library         Browser
Library         Collections
Library         OperatingSystem
Library         DateTime
Library         libs/DeloreanLibrary.py

Resource        actions/auth.robot
Resource        actions/nav.robot
Resource        actions/students.robot
Resource        actions/components.robot
Resource        actions/plans.robot
Resource        actions/enrolls.robot

Resource        helpers.robot

***Keywords***
Start Browser Session
    New Browser     ${browse}    ${headless}
    New Page        about:blank

User Session
    ${user}   Create Dictionary     email=admin@bodytest.com    pwd=pwd123
    Start Browser Session
    Go To Login Page
    Login With                   ${user.email}     ${user.pwd}
    User Should Be Logged In     Administrador

Clear LS
    Take Screenshot
    LocalStorage Clear

ThinkTime
    ${timeout}  Set Variable     2
    Sleep           ${timeout}
    Take Screenshot


 